import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host="localhost", database="METEO", user="andrea", password="")

    if connection.is_connected():
        dbInfo = connection.get_server_info()
        print("CONNECTED TO MYSQL SERVER VERSION ", dbInfo)
        cursor = connection.cursor()    #mi restituisce a quale DB sono connesso
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You are connected to DB: ", record)

except Error as e:
    print("Errore durante la connessione al DB ", e)
finally:
    if connection.is_connected():
        query = "SELECT * FROM Station"
        cursor.execute(query)
        for (DataOdierna, Ora, Temperatura, Umidita, Pressione) in cursor:
            print("Data Odierna: {}".format(DataOdierna))
            print("Ora: {}".format(Ora))
            print("Temperatura: {}".format(Temperatura))
            print("Umidita: {}".format(Umidita))
            print("Pressione: {}".format(Pressione))
        cursor.close()
        connection.close()
        print("Connessione al server MySQL interrotta")


#The MySQLCursor class instantiates objects that can execute operations such as SQL statements.
# Cursor objects interact with the MySQL server using a MySQLConnection object.
